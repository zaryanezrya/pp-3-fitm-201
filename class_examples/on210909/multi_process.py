import time
from multiprocessing import Process

COUNT = 100000000

def countdown(n):
    while n>0:
        n -= 1

t1 = Process(target=countdown, args=(COUNT//2,))
t2 = Process(target=countdown, args=(COUNT//2,))

start = time.time()
t1.start()
t2.start()
t1.join()
t2.join()
end = time.time()

print('Time taken in seconds -', end - start)