def gen_multiply(a, b):
    while True:
        yield a * b

print(gen_multiply(3, 7))
print(next(gen_multiply(3, 7)))
print(gen_multiply(3, 7).__next__())