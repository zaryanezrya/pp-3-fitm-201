import unittest
import time
import asyncio

from class_examples.on210923.async_get_sites import download_all_sites


#@unittest.skip
class Test210902Async(unittest.TestCase):
    def setUp(self):
        self.start_time = time.time()

    def test_100(self):
        self.sites = [
            "https://www.python.org",
            "https://google.com",
        ] * 50

        res = asyncio.get_event_loop().run_until_complete(download_all_sites(self.sites))
        self.assertEqual(len(self.sites), len(res))

    def tearDown(self):
        duration = time.time() - self.start_time
        print(f"{self.id()}: Downloaded {len(self.sites)} in {duration} seconds")
